
import java.util.*;

public class Board {

    private char data[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player atnowPlayer;
    private Player O;
    private Player X;
    private Player win;
    private int turn;
    Random randomstart = new Random();

    public Board(Player O, Player X) {
        this.O = O;
        this.X = X;
        if (randomstart.nextInt(2) + 1 == 1) {
            atnowPlayer = O;
        } else {
            atnowPlayer = X;
        }
        win = null;
    }

    public void setXY(int Xway, int Yway) {
        if (data[Xway - 1][Yway - 1] == ('-')) {
            data[Xway - 1][Yway - 1] = atnowPlayer.getName();
            turn++;
        }
    }

    public Player getPlayer() {
        return atnowPlayer;
    }

    public int getTurn() {
        return turn;
    }

    public char[][] getData() {
        return data;
    }

    public boolean winGame() {
        for (int i = 0; i < 3; i++) {
            if (checkRow(i)) {
                return true;
            } else if (checkCol(i)) {
                return true;
            } else if (checkLeft()) {
                return true;
            } else if (checkRight()) {
                return true;
            }
        }
        switchTurn();
        return false;
    }

    private boolean checkRow(int Xway) {
        for (int k = 0; k < 3; k++) {
            if (data[Xway][k] != atnowPlayer.getName()) {
                return false;
            }
        }
        win = atnowPlayer;
        setWin();
        return true;
    }

    private boolean checkCol(int Yway) {
        for (int l = 0; l < 3; l++) {
            if (data[l][Yway] != atnowPlayer.getName()) {
                return false;
            }
        }
        win = atnowPlayer;
        setWin();
        return true;
    }

    private boolean checkLeft() {
        for (int m = 0; m < 3; m++) {
            if (data[m][m] != atnowPlayer.getName()) {
                return false;
            }
        }
        win = atnowPlayer;
        setWin();
        return true;
    }

    private boolean checkRight() {
        for (int n = 0; n < 3; n++) {
            if (data[2 - n][0 + n] != atnowPlayer.getName()) {
                return false;
            }
        }
        win = atnowPlayer;
        setWin();
        return true;
    }

    public void switchTurn() {
        if (atnowPlayer == O) {
            atnowPlayer = X;
        } else {
            atnowPlayer = O;
        }
    }

    public boolean drawGame() {
        if (turn == 9) {
            win = null;
            O.draw();
            X.draw();
            return true;
        }
        return false;
    }

    public Player getWin() {
        return win;
    }

    public void setWin() {
        if (win == O) {
            O.win();
            X.lose();
        } else if (win == O) {
            X.win();
            O.lose();
        }
    }

}
