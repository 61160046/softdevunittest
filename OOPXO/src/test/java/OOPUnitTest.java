/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OOPUnitTest {

    public OOPUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testcheckRow() {
        Player O = new Player('O');
        Player X = new Player('X');
        Board board = new Board(O, X);
        board.setXY(1, 1);
        board.setXY(1, 2);
        board.setXY(1, 3);
        assertEquals(true, board.winGame());
    }
    public void testcheckRow1() {
        Player O = new Player('O');
        Player X = new Player('X');
        Board board = new Board(O, X);
        board.setXY(2, 1);
        board.setXY(2, 2);
        board.setXY(2, 3);
        assertEquals(true, board.winGame());
    }
    public void testcheckRow2() {
        Player O = new Player('O');
        Player X = new Player('X');
        Board board = new Board(O, X);
        board.setXY(3, 1);
        board.setXY(3, 2);
        board.setXY(3, 3);
        assertEquals(true, board.winGame());
    }
   

    public void testcheckCol() {
        Player O = new Player('O');
        Player X = new Player('X');
        Board board = new Board(O, X);
        board.setXY(1, 1);
        board.setXY(2, 1);
        board.setXY(3, 1);
        assertEquals(true, board.winGame());

    }
     public void testcheckCol1() {
        Player O = new Player('O');
        Player X = new Player('X');
        Board board = new Board(O, X);
        board.setXY(1, 2);
        board.setXY(2, 2);
        board.setXY(3, 2);
        assertEquals(true, board.winGame());
    }
     public void testcheckCol2() {
        Player O = new Player('O');
        Player X = new Player('X');
        Board board = new Board(O, X);
        board.setXY(1, 3);
        board.setXY(2, 3);
        board.setXY(3, 3);
        assertEquals(true, board.winGame());
    }
     

    public void testcheckLeft() {
        Player O = new Player('O');
        Player X = new Player('X');
        Board board = new Board(O, X);
        board.setXY(1, 1);
        board.setXY(2, 2);
        board.setXY(3, 3);
        assertEquals(true, board.winGame());

    }

    public void testcheckRight() {
        Player O = new Player('O');
        Player X = new Player('X');
        Board board = new Board(O, X);
        board.setXY(1, 3);
        board.setXY(2, 2);
        board.setXY(3, 1);
        assertEquals(true, board.winGame());

    }

    public void testSwitchPlayer() {
        Player O = new Player('O');
        Player X = new Player('X');
        Board board = new Board(O, X);
        board.switchTurn();
        assertEquals('X', board.getPlayer().getName());
    }

    public void testDraw() {
        Player O = new Player('O');
        Player X = new Player('X');
        Board board = new Board(O, X);
        board.setXY(1, 1);
        board.switchTurn();
        board.setXY(1, 2);
        board.switchTurn();
        board.setXY(1, 3);
        board.switchTurn();
        board.setXY(2, 2);
        board.switchTurn();
        board.setXY(2, 1);
        board.switchTurn();
        board.setXY(3, 3);
        board.switchTurn();
        board.setXY(3, 1);
        board.switchTurn();
        board.setXY(3, 2);
        board.switchTurn();
        board.setXY(2, 3);
        assertEquals(true, board.drawGame());
    }
    
    
}
